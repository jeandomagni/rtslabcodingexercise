﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodingExcercise.Core.Interfaces;
using CodingExcercise.Infrastucture.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CodingExcercise
{
    class Program
    {
        private static IServiceProvider _serviceProvider;
        static void Main(string[] args)
        {
            RegisterServices();
            IServiceScope scope = _serviceProvider.CreateScope();
            scope.ServiceProvider.GetRequiredService<ConsoleApplication>().Run();
            DisposeServices();        
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<ICompareService, CompareService>();
            services.AddSingleton<IStringService, StringService>();
            services.AddSingleton<ConsoleApplication>();
            _serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}
