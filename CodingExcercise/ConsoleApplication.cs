﻿using CodingExcercise.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExcercise
{
    class ConsoleApplication
    {
        private readonly ICompareService _arrayService;
        private readonly IStringService _stringService;
        public ConsoleApplication(ICompareService arrayService, IStringService stringService)
        {
            _arrayService = arrayService;
            _stringService = stringService;
        }

        public void Run()
        {
            var list = new List<int> { 1, 5, 2, 1, 10 };
            var comparisonValue = 6;

            Console.WriteLine("Start aboveBelow function");

            var result = _arrayService.AboveBelow(list, comparisonValue);

            var input = string.Join(",", list);
            Console.WriteLine($"Input: [{input}], {comparisonValue}");           
            Console.WriteLine("Output:");
            foreach (var item in result)
            {

                var str = $"{item.Key} : {item.Value}";
                Console.WriteLine(str);
            }
            Console.WriteLine("End aboveBelow function");
            Console.WriteLine("-----------------------");
            Console.WriteLine("Start stringRotation fuction");

            var stringToRotate = "MyString";
            var rotationAmount = 2;
            var stringRotationResult = _stringService.StringRotation(stringToRotate, rotationAmount);
           

            Console.WriteLine($"Input: {stringToRotate}, {rotationAmount}");
            Console.WriteLine($"Output: {stringRotationResult}");
            Console.WriteLine("End stringRotation fuction");
        }
    }
}
