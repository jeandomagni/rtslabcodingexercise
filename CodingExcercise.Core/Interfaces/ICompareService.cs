﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExcercise.Core.Interfaces
{
    public interface ICompareService
    {
        Dictionary<string, int> AboveBelow(List<int> list, int comparisonValue);       
    }
}
