﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExcercise.Core.Interfaces
{
    public interface IStringService
    {
        string StringRotation(string str, int rotationAmount);
    }
}
