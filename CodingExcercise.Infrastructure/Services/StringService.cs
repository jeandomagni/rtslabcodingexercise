﻿using CodingExcercise.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExcercise.Infrastucture.Services
{
    public class StringService: IStringService
    {
        public string StringRotation(string str, int rotationAmount)
        {
            if (rotationAmount > str.Length)
            {
                rotationAmount = rotationAmount % str.Length;
            }
            return str[(str.Length - rotationAmount)..] + str[..(str.Length - rotationAmount)];
        }
    }
}
