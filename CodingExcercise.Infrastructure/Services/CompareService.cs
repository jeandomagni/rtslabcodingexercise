﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodingExcercise.Core.Interfaces;

namespace CodingExcercise.Infrastucture.Services
{
    public class CompareService: ICompareService
    {
        public Dictionary<string, int> AboveBelow(List<int> list, int comparisonValue)
        {
            var above = list.Where(x => x > comparisonValue).Count();
            var below = list.Where(x => x < comparisonValue).Count();

            return new Dictionary<string, int> {
                { "above", above},
                { "below", below }
            };
        }        
    }
}
